if ($args.Length -eq 0)
{
    echo "create_key_pair [key name]"
}
else
{
    aws ec2 create-key-pair --key-name $args[0] | Out-File $($args[0] + ".pem")
 }