
Param(
  [string]$os = "Ubuntu",
  [string]$instance = "t2.micro",
  [string]$key = "xerxys",
  [string]$security = "webFW"
 
)
$free_tier = @{"Ubuntu" = "ami-96f1c1c4"}
$free_tier.add("Amazon","ami-68d8e93a")
$free_tier.add("RedHat", "ami-dc1c2b8e")
$free_tier.add("Suse", "ami-84b392d6")
$free_tier.add("Windows2012", "ami-f2d6eca0")
$free_tier.add("Windows2008", "ami-c2d5ef90")

aws ec2 run-instances --image-id $free_tier[$os] --count 1 --instance-type $instance --key-name $key --security-groups $security | out-file instance.csv