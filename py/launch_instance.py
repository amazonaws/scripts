import boto3
from time import sleep
ubuntu_ami = 'ami-96f1c1c4'
win12_ami = 'ami-e0d1ebb2'
win08_ami = 'ami-c2d5ef90'

key_pair_name = 'winWipro'

instance_type = 't2.micro'

instance_tag = {'Key':'Name','Value':'Ubuntu'}

image_type = ubuntu_ami

client = boto3.client('ec2')

response = client.run_instances(
    ImageId = image_type,
    KeyName = 'winWipro',
    MinCount = 1,
    MaxCount = 1,
    SecurityGroups = ['webFW'],
    InstanceType = instance_type,
    Placement = {
        'AvailabilityZone': 'ap-southeast-1a',
        },
    IamInstanceProfile = {
        'Name' : 'admin',
        },
    )


instance_id = response['Instances'][0]['InstanceId']
client.create_tags(Resources=[instance_id],Tags = [instance_tag])
print('instanceID:'+instance_id)
while True:
    sleep(10)
    s_response = client.describe_instances(
        InstanceIds = [instance_id],
        )
    state = s_response['Reservations'][0]['Instances'][0]['State']['Name']
    print('State:' + state)
    if state == "running":
        break
    
print('State:' + s_response['Reservations'][0]['Instances'][0]['State']['Name'])
print('IP:' + s_response['Reservations'][0]['Instances'][0]['PublicIpAddress'])

kbd = input('Proceed with stopping instance?')

print('Stopping the instance:'+instance_id)
resp = client.stop_instances(InstanceIds=[instance_id])
while True:
    sleep(10)
    s_response = client.describe_instances(
        InstanceIds = [instance_id],
        )
    state = s_response['Reservations'][0]['Instances'][0]['State']['Name']
    print('State:' + state)
    if state == "stopped":
        break
kbd = input('Proceed with start instance?')

print('Starting the instance again:'+instance_id)
resp = client.start_instances(InstanceIds=[instance_id])
while True:
    sleep(10)
    s_response = client.describe_instances(
        InstanceIds = [instance_id],
        )
    state = s_response['Reservations'][0]['Instances'][0]['State']['Name']
    print('State:' + state)
    if state == "running":
        break
    
kbd = input('Proceed with terminating instance?')

print('Terminating the instance :'+instance_id)
resp = client.terminate_instances(InstanceIds=[instance_id])
sleep(60)
s_response = client.describe_instances(
    InstanceIds = [instance_id],
    )

print('State:' + s_response['Reservations'][0]['Instances'][0]['State']['Name'])


