import boto3
import pickle
cl = boto3.client('ec2')
kp = cl.create_key_pair(KeyName='winWipro')
kname = kp['KeyName']
f = open(kname+'.pem', 'w')
f.write(kp['KeyMaterial'])
f.close()

keys = cl.describe_key_pairs()
for key in keys['KeyPairs']:
    print('\n'+key['KeyName'])
